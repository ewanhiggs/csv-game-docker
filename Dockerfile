FROM debian:testing

# File Author / Maintainer
MAINTAINER Ewan Higgs <ewan_higgs@yahoo.co.uk>

ENV DEBIAN_FRONTEND=noninteractive

ENV AWK_DEPS="gawk mawk"
ENV C_DEPS="libcsv-dev"
ENV CPP_DEPS="libboost-dev"
ENV HASKELL_DEPS="ghc libffi-dev libgmp-dev"
ENV JAVA_DEPS="openjdk-11-jdk-headless and maven"
ENV JULIA_DEPS="julia"
ENV LUA_DEPS="lua5.1 luajit luarocks"
ENV OCAML_DEPS="ocaml opam ocaml-findlib"
ENV PHP_DEPS="php7.0"
ENV PYTHON2_DEPS="python python-dev python-pip"
ENV PYTHON3_DEPS="python3 python3-dev python3-pip"
ENV R_DEPS="r-base r-base-dev libopenblas-base"
ENV RUBY_DEPS="ruby ruby-dev"

# General tools
RUN apt-get update && \
    apt-get install \
       ca-certificates \
       curl \
       dirmngr \
       gcc \
       gnupg \
       libc6-dev \
       m4 \
       git \
       sudo \
       swig \
       $AWK_DEPS \
       $CPP_DEPS \
       $C_DEPS \
       $HASKELL_DEPS \
       $JAVA_DEPS \
       $JULIA_DEPS \
       $LUA_DEPS \
       $OCAML_DEPS \
       $PHP_DEPS \
       $PYTHON2_DEPS \
       $PYTHON3_DEPS \
       $RUBY_DEPS \
       $R_DEPS \
       -qqy \
       --no-install-recommends

# Setup Crystal repository
RUN apt-key adv --keyserver keys.gnupg.net --recv-keys 09617FD37CC06B54; \
      echo "deb https://dist.crystal-lang.org/apt crystal main" > /etc/apt/sources.list.d/crystal.list

# Install Crystal
RUN apt-get -q update
RUN apt-get -q install --no-install-recommends -y crystal

# Haskell - which also calls apt-get so this must happen before we clean apt
# caches
WORKDIR /
RUN curl -sSL https://get.haskellstack.org/ | sh

# Clean the apt-caches.
RUN rm -rf /var/lib/apt/lists/*

# Create a user with which to do the rest of the work.
# Ocaml requires this - but wercker must run as root, so basically wercker can't
# work with ocaml.
#RUN useradd developer -d /home/developer -m -s /bin/bash && \
#    echo developer:developer | chpasswd && \
#        usermod -a -G sudo developer

# Lua
RUN luarocks install lpeg

#USER developer

# Python
RUN pip install pandas
# Perl
# Perl seems to install Text::CSV_XS just fine but still gives error code 8.
RUN cpan install perl Text::CSV_XS ; exit 0
# OCaml
RUN opam init -a && eval $(opam config env) && opam update && opam install -y csv

USER root

# R
RUN R -e 'install.packages("data.table")'

#Rust
RUN mkdir /rust
RUN pwd
WORKDIR /rust

# Rust
ENV RUST_ARCHIVE=rust-nightly-x86_64-unknown-linux-gnu.tar.gz
ENV RUST_DOWNLOAD_URL=https://static.rust-lang.org/dist/$RUST_ARCHIVE

RUN curl -fsOSL $RUST_DOWNLOAD_URL \
    && curl -s $RUST_DOWNLOAD_URL.sha256 | sha256sum -c - \
    && tar -C /rust -xzf $RUST_ARCHIVE --strip-components=1 \
    && rm $RUST_ARCHIVE \
    && ./install.sh

# Ruby
RUN gem install fastest-csv

# Clojure
ENV LEIN_ROOT 1

# Install Leiningen and make executable
ENV LEIN_DOWNLOAD_URL=https://raw.githubusercontent.com/technomancy/leiningen/2.6.1/bin/lein
RUN curl -s $LEIN_DOWNLOAD_URL > /usr/local/bin/lein \
    && chmod 0755 /usr/local/bin/lein \
    && /usr/local/bin/lein

# Nim
WORKDIR /nim
ENV NIM_VERSION=0.19.9
ENV NIM_ARCHIVE=nim-$NIM_VERSION.tar.xz
ENV NIM_DOWNLOAD_URL=https://nim-lang.org/download/$NIM_ARCHIVE
RUN curl -fsOS $NIM_DOWNLOAD_URL \
    && curl -s $NIM_DOWNLOAD_URL.sha256 | sha256sum -c - \
    && tar -C /nim -xf $NIM_ARCHIVE \
    && rm $NIM_ARCHIVE \
    && cd nim-$NIM_VERSION \
    && sh build.sh \
    && bin/nim c koch \
    && ./koch tools
ENV PATH="${PATH}:/nim/nim-${NIM_VERSION}/bin"

# Golang
WORKDIR /usr/local
RUN curl -s https://dl.google.com/go/go1.12.4.linux-amd64.tar.gz > go.tar.gz \
    && tar xzf go.tar.gz \
    && rm go.tar.gz
ENV PATH="${PATH}:/usr/local/go/bin"

# Paratext
WORKDIR /paratext
RUN pip install numpy six \
    && git clone https://github.com/wiseio/paratext.git \
    && cd paratext/python \
    && python setup.py install

WORKDIR /
#USER developer
